+++
date = "2016-11-25T22:32:32+01:00"
title = "Créer son entreprise : les débuts"
author = "Tristram"
+++

_Codeurs en Liberté_ a été créée il y a quatre mois et depuis deux
mois nous versons un salaire.

Voici un petit retour d’expérience qui pourra intéresser toute
personne devant créer son entreprise et s’interrogeant sur la
difficulté d’une telle aventure.

**Spoilers :** ça n’a rien de sorcier et demande juste un peu d’organisation.

## Les statuts

Sans rentrer dans le détail des formes juridiques, la forme la plus
passe-partout est la SAS (_Société par actions simplifiée_).

Un avantage siginifactif est que le président peut être salarié et donc
dépendre du régime général, et non pas du RSI.

Je conseille vivement d’avoir un capital variable. Cela veut dire que si
un nouvel associé veut entrer dans l’entreprise, de nouvelles parts
pourront être créées pour lui, sans devoir déposer le changement. Cela
économise la paperasse et les frais associés.

### La raison sociale

Faites attention à bien décrire votre activité. En effet, l’INSEE vous
attribuera automatiquement un numéro [APE](https://www.insee.fr/fr/metadonnees/definition/c1507). Il
est défini à des fins purement statistiques.

Cependant, ce numéro sera transmis automatiquement à d’autres organismes.
Cela peut influencer sur la convention collective dans laquelle vous
serez automatiquement classé, ou encore sur le taux à payer à l’assurance
maladie au titre du risque des accidents du travail.

Être mal catégorisé n’est pas une fatalité, mais vous pouvez économiser quelques courriers
en indiquant « _Développement en informatique_ » et non pas
« _Formation et développement en informatique_ » si la formation est
juste une activité annexe envisagée.

### Le coup de main

Nous sommes passés par [Legalstart](https://www.legalstart.fr), mais il
existe d’autres services équivalents, probablement de la même efficacité.
Nous avons utilisé des statuts génériques. En effet, il est très simple
de les adapter ultérieurement à vos besoins précis.

Bref, faites au plus simple et pensez au capital variable pour ne pas
vous retrouver coincés par la suite.

## Banque

Pour créer l’entreprise, il faudra déposer l’argent du capital initial auprès d’une banque
qui conserve l’argent jusqu’à l’enregistrement en bonne et due forme.

L’unique banque éthique en France, [La Nef](https://lanef.com), ne permet pas encore
le dépôt de capital initial. Espérons que cela change bientôt.

Aucune banque en ligne (telle que Boursorama, ING Direct ou Fortuneo) ne
fait de compte professionnel.

En attendant que les petits nouveaux tels que [Qonto](https://qonto.eu) ou
[IbanFirst](https://www.ibanfirst.com) soient prêts, il faudra donc travailler
avec une banque traditionnelle.

Je n’ai trouvé strictement aucun avantage pour l’une ou l’autre. Le tarif
sera toujours autour de 30€/mois pour un chéquier, une carte bancaire et un
site web pour voir l’état des comptes et faire des virements. Certaines banques
seront moins chères, mais vous factureront des frais de mouvement.

Bref, allez à la banque la plus proche de chez vous. La mienne a nécessité
deux rendez-vous physiques de 30 minutes et 1 heure.

## La comptabilité et paye

Les deux sujets viennent ensemble. Il n’y a pas besoin d’avoir un
comptable, sauf si vous faites une phobie aux papiers, ou si vous
n’avez pas de temps à y consacrer. Alors il sera très
utile.

Nous utilisons [Zefyr](https://www.zefyr.net), qui derrière son aspect un peu
vieillot fait très bien l’affaire. Nous nous en servons pour :

* la comptabilité,
* la facturation,
* la paye.

Une autre alternative envisagée était [Payfit](https://payfit.com) qui
coûte assez cher et ne fait que la paye. Par contre, ils se chargent de
faire les déclarations et versements aux organismes sociaux à votre place.

Enfin [Fred de la Compta](https://app.freddelacompta.com/), propose de faire toute
la comptabilité ainsi que la paye à votre place.
Le prix s’en ressentira, mais si
vous n’avez pas la rigueur pour archiver régulièrement vos notes de frais,
que le fonctionnement de la TVA vous emmerde, et que le _plan comptable_
vous rappelle juste des mauvais souvenirs de cours en école, alors c’est
peut-être une bonne solution.

## Les partenaires sociaux

La France a choisi historiquement que c’est le travail qui donne droit à une
protection sociale. C’est donc l’employeur qui doit payer la contribution
à chacune des assurances (vieillesse, maladie, chômage…).

Heureusement, malgré la complexité d’une feuille de paye, un effort
considérable a été fait pour simplifier la vie de l’employeur.
Il ne faut verser les contributions qu’à deux organismes collecteurs :

* l’URSSAF pour l’essentiel,
* un organisme privé pour les retraites complémentaires obligatoires.

Ne vous inquiétez pas, ils vous contacteront directement.

## Médecine du travail

Il semblerait que le plus simple soit de choisir la plus proche de
votre lieu de travail.

Les prix et les relations semblent être à peu près homogènes.

## Mutuelle et prévoyance

Elles sont obligatoires. Certains organismes proposent les deux.

[Alan](https://alan.eu) veut simplifier la mutuelle. La protection
est plus faible que celle que j’ai pu avoir dans mes emplois précédents,
mais la simplicité et le prix adapté en conséquence peuvent vous
intéresser.

En ce qui concerne la prévoyance, rien n’a retenu mon attention.

## Les arnaques et SPAM

Attendez-vous à vous faire spammer par la poste. Du fournisseur de trombones
à Google qui veut vous vendre des emplacements publicitaires, votre boite
aux lettres sera régulièrement pleine.

Par contre méfiez-vous des arnaques. Vous recevrez des appels de fonds pour payer
afin d’apparaitre dans un annuaire au nom qui sonne officiel. Il n’en est rien !

## Pense-bête

Avec un peu de méthode et de rodage, j’estime que la gestion courante
nécessite environ une journée par mois. Voici à titre indicatif et très
grossier les actions effectuées pour gérer l’entrerprise :

* **Au fil de l’eau** : scanner et archiver en ligne les courriers et factures… (12 fois 20 minutes) ;
* **En fin de mois** : faires les feuilles de paye, faire le virement (30 minutes par employé) ;
* **En début de mois**
 * Pointer les relevés de compte avec le logiciel de comptabilité et les justificatifs archivés (2 heures),
 * Faire la DSN 15 minutes ;
* **Une fois par trimestre** : déclaration et paiement des partenaires sociaux (2 heures) ;
* **Une fois par an**
 * Impôts et TVA,
 * Bilan,
 * Assemblée générale ordinaire.

## Obtenir de l’aide

JFGI, ça marche bien. En particulier le site officiel
[Service Public](https://www.service-public.fr) est très clair et expose bien
les différentes obligations.

D’autres sites exposent bien plus en détail les potentielles
subtilités pour chaque démarche.

## Conclusion

Il est encore très tôt pour se faire une idée précise du travail
que représente la gestion d’une entreprise.

En particulier, nous n’avons pas eu de bilan annuel à faire,
d’audit par un organisme social et nous ne sommes jamais tombé
dans un trou noir d’incompréhension administrative.

Peut-être que nous avons eu de la chance ou que je me suis trouvé
une vocation improbable d’administrateur. Mais je pense que la
seule qualité requise est un peu de rigueur et l’admninistration
française n’est vraiment pas si terrible que le veut sa réputation
d’un autre âge.
