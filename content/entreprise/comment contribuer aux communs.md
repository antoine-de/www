+++
date = "2018-04-05T10:02:32+01:00"
title = "Comment contribuer aux communs ?"
author = "Pierre"
+++

Si la majorité des développeuses et développeurs utilise tous les jours des logiciels et bibliothèques libres, il est de plus en plus difficile d'ignorer que ces outils sont la plupart du temps écrits et maintenus par des bénévoles.

Et pourtant, dans le feu de l'action, il n'est pas facile de trouver du temps pour contribuer aux projets qu'on utilise, de passer du temps à isoler et remonter un bogue, ou de contribuer à une nouvelle fonctionnalité plutôt que de l'écrire uniquement dans son propre code.

Les réponses à cette question du financement de l'open-source (et des communs en général) sont multiples. À Codeurs en Liberté, nous avons tenté d'en explorer une : financer nos propres contributions.

Voici donc un premier retour d'expérience concret sur notre « pot à cookies ».

## Le pot à cookie 🍪

Depuis sa fondation, Codeurs en Liberté met chaque mois de l'argent de côté dans
un « pot à cookies ». À quoi sert-il ? L'idée est qu'un membre de la coopérative puisse proposer de prendre quelques jours pour contribuer à un commun, et en particulier à un projet open-source. Si sa proposition est acceptée, il sera alors payé pour ses jours de travail en prélevant dans le pot à cookies.

Au début de l'année, j'ai expérimenté ce fonctionnement en proposant de contribuer à l'intégration de [Mattermost](https://about.mattermost.com/) (une alternative open-source à Slack) dans la liste des applications officielles de [Yunohost](https://yunohost.org/) (qui permet de gérer facilement des applications auto-hébergées).

J'ai donc ouvert [une issue](https://gitlab.com/CodeursEnLiberte/fondations/issues/33) pour proposer l'idée, en précisant l'objectif : améliorer la robustesse du paquet déjà existant, faire des tests supplémentaires pour s'assurer de la fiabilité du code, et initier le processus d'inclusion du paquet dans la liste des applications officielles de Yunohost, pour lui donner plus de visibilité et d'impact.

Pour ce travail j'avais compté trois jours, en pensant que la majeure partie du temps serait fragmentée en attente de revues de code, en interactions sociales, etc.

## Ce qui s'est passé

La proposition a été acceptée par les membres : j'ai donc commencé à nettoyer le code, rajouter des fonctionnalités manquantes (comme la possibilité de changer le domaine utilisé par l'application). J'ai également écrit une présentation de l'application sur les forums de Yunohost, en demandant des tests aux utilisateurs intéressés. Ça a été utile : faire fonctionner le paquet dans d'autres contextes a permis de trouver et de corriger des pépins lors de la mise à jour d'une version à l'autre.

Et puis au fur et à mesure, deux questions sont apparues :

- Certaines améliorations demandaient des modifications dans Mattermost lui-même. Bien sûr il était possible de contourner le problème dans les scripts de packaging, mais une solution dans l'application elle-même serait clairement meilleure.
- Il est apparu que Yunohost évite en règle générale de promouvoir des applications officielles sans support de l'authentification unique (LDAP, ou mieux encore SSO). Et le LDAP n'est pas disponible dans la version libre de Mattermost.

Au vu de cela, plutôt que de passer plus de temps sur les scripts de Yunohost, il avait l'air plus pertinent de concentrer le travail sur l'amélioration de l'application elle-même.

## Contribuer à Mattermost

Un petit sous-projet, donc : améliorer le démarrage du serveur de Mattermost, remonter proprement les erreurs critiques au démarrage plutôt que de les ignorer, et intégrer tout ça proprement avec `systemd` s'il est présent.

Première bonne surprise : la mise en place d'un environnement de développement pour Mattermost est assez facile. La documentation est claire, et les outils soignés : alors que j'avais des contraintes compliquées (tester une fonctionnalité propre à Linux tout en développant sous un Mac), il ne m'a fallu qu'une ou deux heures pour mettre en place les briques nécessaires.

Autre bonne surprise : même sans avoir écrit de Go auparavant, le code est lisible et facile à suivre. Vu que Go est particulièrement conçu pour être un langage simple et sans finasseries, j'imagine que c'est en bonne partie lié au langage lui-même.

J'ai donc écrit un peu de code, ronchonné un moment sur le manque de tests concernant cette partie en particulier, rajouté les tests en question, et envoyé [une première contribution](https://github.com/mattermost/mattermost-server/pull/8189). Plus qu'à attendre. Après tout, il n'est pas rare qu'une contribution à un projet open-source reste sans réponse pendant plusieurs jours ou semaines, même quand des personnes travaillent dessus à plein temps.

Bonne surprise à nouveau : la première revue de code est arrivée en quelques heures. Et le premier patch a été accepté directement, et mergé sans remarques particulières.

J'ai continué à envoyer [des contributions](https://github.com/mattermost/mattermost-server/pulls?utf8=%E2%9C%93&q=is%3Apr+author%3Akemenaran+) – qui ont toutes été relues sans délai et acceptées, avec tout au plus quelques modifications mineures. Au final, Codeur en Liberté a même été nommé « [contributeur du mois](https://www.mattermost.org/mvp/) » 🤘

Bref, le processus de contribution à Mattermost a été une expérience étonnamment agréable. Les relectures rapides et sans pinaillage inutile de mes premiers commits m'ont vraiment incité à envoyer les contributions suivantes ; je ne sais pas si je serais allé jusqu'au bout sinon.

## Des modèles qui se croisent

Au fond, rien que sur ce petit bout de projet, on peut observer plusieurs modèles de contribution au logiciel libre qui se croisent :

- Yunohost, maintenu par des bénévoles,
- Codeurs en Liberté, qui sponsorise quelques jours de travail salarié,
- Mattermost, une société commerciale qui emploie des salariés à plein temps.

Et les frontières ne sont pas si nettes : à Codeurs en Liberté on effectue aussi parfois du travail bénévole (par exemple pour développer nos outils internes). Et il semble que si un bon tiers du travail sur Mattermost est effectué par les employés, un autre tiers vient de clients commerciaux de Mattermost, qui en plus de payer pour le produit contribuent à l'améliorer – et un dernier tiers de contributeurs individuels.

Si cette variété de modèles me semble riche et intéressante, au fond aucun ne me convainc parfaitement. Le modèle bénévole permet difficilement d'avancer vite, et court le risque de se fonder essentiellement sur la bonne volonté et le temps disponible des contributeurs. Une entreprise qui maintient un produit seulement en partie libre court le risque de s'orienter plus vers le profit, et d'exploiter le travail gratuit des contributeurs occasionnels. Et même la contribution ponctuelle via notre pot à cookies a ses limites : manifestement il nous est difficile de proposer des contributions avec ce mécanisme. Peut-être est-il difficile de cadrer quelques jours de travail de cette manière, ou qu'on ne se sent pas assez légitime pour explorer une base de code inconnue tout en étant rémunéré ?

## Pour la suite

En en discutant entre membres de Codeurs en Liberté ces dernières semaines, on se demandait s'il ne serait pas plus pertinent d'organiser des périodes de contribution plus longues et plus collectives.

Une idée à l'étude est d'organiser une « **semaine de contribution aux communs** ». Pendant toute une semaine, on se réunirait avec plusieurs participants dans un espace commun, et on travaillerait sur un projet open-source ou open-data. Regrouper des gens dans l'espace et dans le temps permettrait sans doute de se motiver, de mettre le pied à l'étrier pour des personnes qui ne contribuent pas d'habitude, et de s'entraider plus facilement.

Et si ça vous intéresse, [on en discute ici](https://gitlab.com/CodeursEnLiberte/fondations/issues/45).
