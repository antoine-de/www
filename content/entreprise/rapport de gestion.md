+++
date = "2018-01-25T22:32:32+01:00"
title = "Rapport de gestion 2016—2017"
author = "Tristram"
+++

Pendant deux jours, nous sommes allés à Orléans pour prendre l’air,
passer du bon temps, et surtout faire le bilan, _calmement_, et réfléchir à l’avenir.

Ce qui suit est le rapport de gestion qui a été présenté et que nous transcrivons
par volonté de transparence.

Plus généralement, nos compte rendus sont lisibles sur notre [dépot _fondations_](https://gitlab.com/CodeursEnLiberte/fondations/tree/master/comptes%20rendus).

# Rapport de gestion concernant les opérations de l'exercice clos le 31/12/2017

Cher·e·s sociétaires, chers curieux et chères curieuses,

Conformément à la législation, je me plie à l’exercice du rapport de gestion pour
l’exercice 2016—2017 clos le 31 décembre 2017.

Étant donné notre fonctionnement, vous avez accès en tant que sociétaire à l’ensemble des données
comptables en toute transparence toute l’année.

La liasse fiscale est [jointe](/bilan/liasse_fiscale_2016.pdf) à ce document.

## 1. Situation et activité de la société durant l'exercice écoulé

Codeurs en Liberté a été formellement créée en juillet 2016.

Ce premier exercice a donc duré 18 mois et a été la preuve que notre entreprise fonctionne
et que nos choix tiennent la route.

### Bilan financier

La liasse fiscale fait foi et c’est elle qui sera soumise à votre approbation.

Cependant, voici un résumé de l’exercice passé (en milliers d’euros) : 

* **Chiffre d’affaires** : 395,23
* **Salaire immédiat** (net et abondement pour l’épargne salariale) : 185,75
* **Salaire sociabilisé** : 146,77
* **Dépenses communes** (y compris complémentaire santé et prévoyance) : 15,68
* **Frais individuels** : 3,59
* **Dons** : 2,00
* **Résultat brut** : 41,44
* **Impôts** : 6,21
* **Résultat net**: 35,22

### Les membres

De trois personnes à la création, nous sommes désormais 6 membres à la fois salariés et sociétaires.

Plusieurs personnes ont manifesté leur intérêt pour notre structure ou simplement
manifesté leur sympathie pour le projet.

Il y a donc un véritable attrait à des formes alternatives à la subordination et qui
développent la solidarité.

Personne n’a rencontré de problème pour facturer le minimum nécessaire au fonctionnement,
ni pour faire sa part du travail administratif.

### Clients

Nos clients ont été les suivants, par ordre de chiffre d’affaires décroissant :

* [Etalab](http://www.etalab.gouv.fr/)/[beta.gouv](https://beta.gouv.fr/) (au travers de [Octo Technology](https://www.octo.com/))
* [La Compagnie des Mobilités](http://about.geovelo.fr/)
* [eQualit.ie](https://equalit.ie/)
* [Deezer](http://www.deezer.com/)
* [Transdev](https://www.transdev.com/) (au travers de [15marches](https://15marches.fr/))
* [Île-de-France Mobilités](https://www.iledefrance-mobilites.fr/) (au travers de 15marches)
* [Karos](https://www.karos.fr/)
* [Île-de-France Mobilités](https://www.iledefrance-mobilites.fr/) (au travers de [Five-by-Five](http://www.fivebyfive.io/))
* [Région Bretagne](http://www.bretagne.bzh/) (au travers de [15marches](https://15marches.fr/))

### Évolution et prespectives d’avenir pour l’exercice 2018

L’exercice passé aura été celui de la découverte de la gestion de l’entreprise, du travail
indépendant, de la collaboration et de la réflexion sur ce que nous souhaitons.

Grâce à cette expérience, nous pourons mieux nous définir et affiner notre projet.

Le projet marquant sera la transformation en coopérative d’un point de vue juridique.

Les autres grands projets incluent l’adaptation des calculs de répartition du chiffre d’affaires
et tisser des liens avec les autres structures à la philosophie proche de la nôtre.

### Évènements importants survenus depuis la clôture

Néant

### Activité de recherche et développement

Néant

### Prise de participation

Néant

### Filiales et participations

Néant

### Dépenses non déductibles fiscalement

Néant

## 3. Conventions réglementées visées à l'article L. 223-19 du Code de commerce

Aucune convention relevant de l'article L. 223-19 du Code de commerce n'a été portée à la connaissance du gérant.

## 4. Affectation du résultat

Le résultat est de 35 224€.

Je propose d’affecter le résultat selon la répartition suivante :

* 15% en réserve, soit 5 283,60 €
* 85% en repport à nouveau, soit 29 940,40€
* 0% en dividendes

## 5. Résolutions particulières

### Choix d’un nouveau président

Mon mandat de président finit avec cet exercice et je ne souhaite pas me représenter à ce poste.

### Prime exceptionnelle

Un accord de participation doit être conclu 6 mois avant la fin de l’exercice. Il n’y a donc pas de participation
cette année.

Je propose que 50% du résultat (soit 17 612€) soit affecté au calcul d’une prime exceptionnelle selon la méthode suivante :

* 275,19€ par salarié et par mois effectivement travaillé
* Le montant sera affecté au chiffre d’affaires de chaque salarié

## Conclusion

J’espère que cette présentation de l’exercice passé ainsi que les résolutions vous conviennent et que
vous me donnerez le _quitus_ pour la gestion de cet exercice.

Tristram Gräbener
